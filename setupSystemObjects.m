function obj = setupSystemObjects(video)

% Video megnyit�sa egy reader objektumba
obj.reader = vision.VideoFileReader(video);

% K�t lej�tszt� ablakot hozunk l�tre: egyiket a video
% lej�tsz�s�ra, m�sikat a k�sz�b�lt objektumok bemutat�s�ra
obj.videoPlayer = vision.VideoPlayer('Position', [0, 100, 650, 380]);
obj.maskPlayer = vision.VideoPlayer('Position', [650, 100, 650, 380]);

% Objektumok az el�t�r detekt�l�s�hoz

% Az el�t�r detektor egy olyan f�ggv�ny, amely a mozg�k�pen a
% k�pkock�k analiz�l�s�val egy bin�ris k�pet k�sz�t, ahol 1
% azon pixelek �rt�ke, amelyek mozg� (el�t�r) objektukmokhoz
% tartoznak, a t�bbi 0.

obj.detector = vision.ForegroundDetector('NumGaussians', 3, ...
    'NumTrainingFrames', 50, 'MinimumBackgroundRatio', 0.7);

% Az �sszetartoz� pixelek megkeres�se, �s az ez�ltal el��ll�
% objektumok jellemz�inek kisz�m�t�sa: befoglal� t�glalap, s�lypont,
% ter�let

obj.blobAnalyser = vision.BlobAnalysis('BoundingBoxOutputPort', true, ...
    'AreaOutputPort', true, 'CentroidOutputPort', true, ...
    'MinimumBlobArea', 3000);
end

