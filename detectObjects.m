function [centroids, bboxes, mask] = detectObjects(obj, frame)

% H�tt�r levon�s
mask = obj.detector.step(frame);

% Morfol�giai sz�r�k haszn�lata
% A nyit�ssal t�r�lj�k a kis zaj objektumokat
% A z�r�ssal 'egybeolvasztjuk' a szakad�sokat
mask = imopen(mask, strel('rectangle', [3,3]));
mask = imclose(mask, strel('rectangle', [20, 20]));
% V�g�l pedig 'bet�mj�k' a lyukakat
mask = imfill(mask, 'holes');

% A legal�bb 1500 pixelnyi �sszef�gg� pixelcsoportokb�l
% objektumokat hozunk l�tre
[~, centroids, bboxes] = obj.blobAnalyser.step(mask);
end