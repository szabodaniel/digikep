function [tracks, carCount] = updateAssignedTracks(tracks, centroids, bboxes, assignments, carCount)

minVisibleCount = 8;
numAssignedTracks = size(assignments, 1);
for i = 1:numAssignedTracks
    trackIdx = assignments(i, 1);
    detectionIdx = assignments(i, 2);
    centroid = centroids(detectionIdx, :);
    bbox = bboxes(detectionIdx, :);
    
    % Jav�tjuk a becsl�st
    correct(tracks(trackIdx).kalmanFilter, centroid);
    
    % A j�solt befoglal� t�glalap helyettes�t�se a detekt�lttal.
    tracks(trackIdx).bbox = bbox;
    
    % N�velj�k a k�vetett objektum kor�t
    tracks(trackIdx).age = tracks(trackIdx).age + 1;
    
    if tracks(trackIdx).totalVisibleCount+1 == minVisibleCount && ...
            tracks(trackIdx).consecutiveInvisibleCount == 0
        % tal�ltunk m�gegyet
        carCount = carCount + 1;
    end
    
    % L�that�s�gi v�ltoz�k friss�t�se: l�that� +1 id�egys�g �ta, �s nem
    % l�that� nulla id�egys�g �ta. �gy biztos�tott, hogy ha egy objektum
    % m�r nem l�that� egy ideje, akkor azt �szrevegy�k.
    tracks(trackIdx).totalVisibleCount = ...
        tracks(trackIdx).totalVisibleCount + 1;
    tracks(trackIdx).consecutiveInvisibleCount = 0;
end
end