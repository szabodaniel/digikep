function tracks = predictNewLocationsOfTracks(tracks)
for i = 1:length(tracks)
    bbox = tracks(i).bbox;
    
    % A jelenleg k�vetett objektum s�lypontja hol lesz
    predictedCentroid = predict(tracks(i).kalmanFilter);
    
    % A j�solt befoglal� t�glalap koordin�t�inak kisz�m�t�sa (bal fels� sarok)
    predictedCentroid = int32(predictedCentroid) - bbox(3:4) / 2;
    % A befoglal� t�glalap koordin�t�inak m�dos�t�sa a j�sl�ssal,
    % sz�less�ge marad, bal fels� sarok v�ltozik
    tracks(i).bbox = [predictedCentroid, bbox(3:4)];
end
end