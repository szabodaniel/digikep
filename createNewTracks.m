function [tracks, nextId] = createNewTracks(tracks, centroids, bboxes, unassignedDetections, nextId)
centroids = centroids(unassignedDetections, :);
bboxes = bboxes(unassignedDetections, :);

for i = 1:size(centroids, 1)
    
    centroid = centroids(i,:);
    bbox = bboxes(i, :);
    
    % Kalm�n sz�r� l�trehoz�sa
    kalmanFilter = configureKalmanFilter('ConstantVelocity', ...
        centroid, [200, 50], [100, 25], 100);
    
    % �j �res strukt�ra az elem tulajdons�gaihoz
    newTrack = struct(...
        'id', nextId, ...
        'bbox', bbox, ...
        'kalmanFilter', kalmanFilter, ...
        'age', 1, ...
        'totalVisibleCount', 1, ...
        'consecutiveInvisibleCount', 0);
    
    nextId = nextId + 1;
    % A v�g�hez hozz��rjuk az �j elemet
    tracks(end + 1) = newTrack;
end
end