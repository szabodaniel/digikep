function [ ] = carCounter( video)% A sz�ks�ges objektumok l�trehoz�sa, video beolvas�sa
    obj = setupSystemObjects(video);

    % A k�vetett objektumok tulajdons�gait tartalmaz� strukt�ra inicializ�l�sa
    tracks = initializeTracks();

    nextId = 1; % kezdeti ID
    carCount = 0; % tal�lt j�rm�vek sz�ma

    % Mozg� objektumok keres�se, sz�ml�l�s.
    while ~isDone(obj.reader)
        frame = readFrame(obj); % egy k�pkocka (k�p) beolvas�sa a video inputb�l
        [centroids, bboxes, mask] = detectObjects(obj, frame); % mozg� objektumokat szegment�ljuk a k�pen
        tracks = predictNewLocationsOfTracks(tracks); % j�sl�s: jelenleg k�vetett objektumok hol lesznek ebben a ciklusban

        % megfeleltetj�k a detekt�lt objektumainkat a jelenleg k�vetetteknek
        [assignments, unassignedTracks, unassignedDetections] = ...
            detectionToTrackAssignment(tracks, centroids);

        % friss�tj�k az objektumadatb�zist a tal�latokkal
        [tracks, carCount] = ...
            updateAssignedTracks(tracks, centroids, bboxes, assignments, carCount);

        % friss�tj�k az objektumadatb�zis nem tal�lt elemeit
        tracks = ...
            updateUnassignedTracks(tracks, unassignedTracks);

        % t�r�lj�k azokat, amiket m�r egy ideje nem l�tunk
        tracks = ...
            deleteLostTracks(tracks);

        % �jakat vesz�nk fel az adatb�zisba
        [tracks, nextId] = ...
            createNewTracks(tracks, centroids, bboxes, unassignedDetections, nextId);

        % eredm�nyek megjelen�t�se
        displayTrackingResults(obj, frame, mask, tracks, carCount);
    end
    fprintf('%d aut�t detekt�ltam.\n',carCount);
end