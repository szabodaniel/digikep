function tracks = updateUnassignedTracks(tracks, unassignedTracks)
for i = 1:length(unassignedTracks)
    ind = unassignedTracks(i);
    % �regedik, de nem l�that�, �gy n�velj�k a m�sik v�ltoz�t is
    tracks(ind).age = tracks(ind).age + 1;
    tracks(ind).consecutiveInvisibleCount = ...
        tracks(ind).consecutiveInvisibleCount + 1;
end
end