function tracks = deleteLostTracks( tracks )
if isempty(tracks)
    return;
end

% k�sz�b�rt�k: ha 10 k�pkock�n �t nem detekt�ljuk, akkor elt�nt
invisibleForTooLong = 10;
% k�sz�b�rt�k: ha kevesebb mint 8 k�pkock�ja l�tjuk, akkor m�g nem biztos,
% hogy val�s objektum, lehet csak hiba
ageThreshold = 8;

% h�nyados: objektum kora / �szlel�sek sz�ma
ages = [tracks(:).age];
totalVisibleCounts = [tracks(:).totalVisibleCount];
visibility = totalVisibleCounts ./ ages;

% Nem j� elemek indexeinek megkeres�se
lostInds = (ages < ageThreshold & visibility < 0.6) | ...
    [tracks(:).consecutiveInvisibleCount] >= invisibleForTooLong;

% Elvesztett elemek t�rl�se
tracks = tracks(~lostInds);
end