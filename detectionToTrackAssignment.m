function [assignments, unassignedTracks, unassignedDetections] = ...
    detectionToTrackAssignment(tracks, centroids)

nTracks = length(tracks);
nDetections = size(centroids, 1);

% K�lts�gm�trix: annak a k�lts�ge, hogy egy detekt�lt objektum egy m�r
% el�z�ekben detekt�lt, �s 'k�vetett' objektum megfelel�je. Magyar�n a
% k�l�nbs�g a mostani detekt�lt objektumok s�lypontja, �s az el�z�ekben
% detekt�ltak 'j�solt' s�lypontjai k�z�tt
cost = zeros(nTracks, nDetections);
for i = 1:nTracks
    cost(i, :) = distance(tracks(i).kalmanFilter, centroids);
end

% Max 20 pixelnyi k�l�nbs�get enged�nk meg
costOfNonAssignment = 20;
% oszt�lyoz�s 3 oszt�lyba: ismert �s megtal�ltuk, ismert viszont nem
% tal�ltuk, illetve a harmadik oszt�ly az abszol�t ismeretlen elemek.
% Ezekb�l hozzuk l�tre az �j bejegyz�seket az adatb�zisba. Kezdetben minden
% detekt�l�s �j elem.
[assignments, unassignedTracks, unassignedDetections] = ...
    assignDetectionsToTracks(cost, costOfNonAssignment);
end